package com.amazonaws.demo.androidpubsub;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataModal {

    /**
     * type : message
     * data : {"volume":"10","brightness":"11"}
     * replyto : default/communication
     */

    private String type;
    private DataBean data;
    private String replyto;

    /**
     * topic : TOPIC_NAME
     * id : Device Id
     */

    private String id;
    /**
     * topic : ["thinggroup/default"]
     * replyTo : undefined/portal
     */

    private String replyTo;
    @SerializedName("topic")
    private List<String> topicX;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getReplyto() {
        return replyto;
    }

    public void setReplyto(String replyto) {
        this.replyto = replyto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public List<String> getTopicX() {
        return topicX;
    }

    public void setTopicX(List<String> topicX) {
        this.topicX = topicX;
    }

    public static class DataBean {
        /**
         * volume : 10
         * brightness : 11
         */

        private String volume;
        private String brightness;

        public String getVolume() {
            return volume;
        }

        public void setVolume(String volume) {
            this.volume = volume;
        }

        public String getBrightness() {
            return brightness;
        }

        public void setBrightness(String brightness) {
            this.brightness = brightness;
        }

    }

}
