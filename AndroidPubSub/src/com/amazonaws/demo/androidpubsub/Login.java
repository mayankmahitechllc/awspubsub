package com.amazonaws.demo.androidpubsub;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;

public class Login extends Activity implements LoginHandler {
    //Request code for Write setting permission activity
    protected static final int REQUEST_WRITE_SETTINGS = 2;

    Button btnLogin;
    EditText etUsername;
    EditText etPassword;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("Login");
        initViewComponents();

        //Asking write setting permission
        askSettingWritePermission();
    }

    private void initViewComponents() {
        btnLogin = findViewById(R.id.btnLogin);
        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        progressBar = findViewById(R.id.progressBar);
        String username = SharedPrefUtils.getStringData(this, SharedPrefUtils.LOGIN_USERNAME);
        String password = SharedPrefUtils.getStringData(this, SharedPrefUtils.LOGIN_PASSWORD);
        etUsername.setText(username);
        etPassword.setText(password);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                Cognito authentication = new Cognito(Login.this);
                if (!TextUtils.isEmpty(etUsername.getText()) && !TextUtils.isEmpty(etPassword.getText()))
                    authentication.userLogin(etUsername.getText().toString().replace(" ", ""), etPassword.getText().toString());
                else
                    Toast.makeText(Login.this, "Please enter username and password", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void loginSuccess() {
        progressBar.setVisibility(View.GONE);
        Intent intent = new Intent(this, PubSubActivity.class);
        startActivity(intent);
    }

    @Override
    public void loginFailure() {
        progressBar.setVisibility(View.GONE);
    }

    private void askSettingWritePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.System.canWrite(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, REQUEST_WRITE_SETTINGS);

                Toast.makeText(this, "Allow Modify System Setting  permission", Toast.LENGTH_LONG).show();
            }
        }
    }
}