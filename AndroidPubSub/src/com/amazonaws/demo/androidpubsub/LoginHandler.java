package com.amazonaws.demo.androidpubsub;

public interface LoginHandler {
    void loginSuccess();
    void loginFailure();
}
