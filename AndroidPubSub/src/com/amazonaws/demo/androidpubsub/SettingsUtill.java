package com.amazonaws.demo.androidpubsub;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.media.AudioManager;
import android.os.Build;
import android.util.Log;

public class SettingsUtill
{

    /**
     * This method is used to set device Brightness the device
     *
     * @param val
     *            Integer value having range from 0 - 100
     */

    public static void setBrightness(Context ctx, int val) {
        if(ctx!=null){
            android.provider.Settings.System.putInt(
                    ctx.getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS,
                    (int) (val * 255/100));
        }

    }

    /**
     * This method is used to set device volume the device
     *
     * @param ctx
     *            A context object of the calling Class (Activity/Service).
     *
     * @param val
     *            Integer value having range from 0 - 100
     */
    public static void setVolume(Context ctx, int val) {
        AudioManager am = (AudioManager) ctx
                .getSystemService(Activity.AUDIO_SERVICE);
        if (am != null) {
            int volume = (val
                    * am.getStreamMaxVolume(AudioManager.STREAM_MUSIC) / 100);
            am.setStreamVolume(AudioManager.STREAM_MUSIC, volume, AudioManager.FLAG_SHOW_UI);
            if (getTouchSound(ctx)) {
                volume = (val * am.getStreamMaxVolume(AudioManager.STREAM_SYSTEM) / 100);
                am.setStreamVolume(AudioManager.STREAM_SYSTEM, volume, AudioManager.FLAG_SHOW_UI);
            }
        }
    }

    /**
     * This method is used to get Touch sound whether enable or disable
     *
     * @param ctx
     *            A context object of the calling Class (Activity/Service).
     * @return Boolean true, false
     */
    private static final String TAG = "settingsUtil";
    public static Boolean getTouchSound(Context ctx) {
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationManager mNotificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
            assert mNotificationManager != null;
            if (mNotificationManager.isNotificationPolicyAccessGranted()) {
                return mNotificationManager.getCurrentInterruptionFilter() == NotificationManager.INTERRUPTION_FILTER_NONE;
            }
        } else {
            AudioManager am = (AudioManager) ctx
                    .getSystemService(Activity.AUDIO_SERVICE);

            try {
                if (am != null) {
                    int audio_mode = am.getRingerMode();
                    if (audio_mode == AudioManager.RINGER_MODE_SILENT) {
                        return false;
                    } else {
                        return true;
                    }
                }
            } catch (Exception e) {
                Log.d(TAG, "Exception: getTouchSound" + e);
            }
        }
        return false;
    }
}
