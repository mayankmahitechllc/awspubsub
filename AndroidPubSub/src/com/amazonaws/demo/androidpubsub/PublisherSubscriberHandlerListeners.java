package com.amazonaws.demo.androidpubsub;

public interface PublisherSubscriberHandlerListeners {
    void receivedMessage(String message,String currentTopicName);
}
