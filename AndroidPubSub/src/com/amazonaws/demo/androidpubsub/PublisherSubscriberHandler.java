package com.amazonaws.demo.androidpubsub;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.amazonaws.mobileconnectors.iot.AWSIotMqttManager;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttNewMessageCallback;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttQos;

import java.io.UnsupportedEncodingException;

import static com.amazonaws.mobile.auth.core.internal.util.ThreadUtils.runOnUiThread;

class PublisherSubscriberHandler {
    private PublisherSubscriberHandlerListeners publisherSubscriberHandlerListeners;

    PublisherSubscriberHandler(Context context) {
        publisherSubscriberHandlerListeners = (PublisherSubscriberHandlerListeners) context;
    }

    @SuppressLint("LongLogTag")
    void subscribeTopic(AWSIotMqttManager mqttManager, final String topicName) {
        try {
            mqttManager.subscribeToTopic(topicName, AWSIotMqttQos.QOS0, new AWSIotMqttNewMessageCallback() {
                @Override
                public void onMessageArrived(final String topic, final byte[] data) {
                    runOnUiThread(new Runnable() {
                        @SuppressLint("LongLogTag")
                        @Override
                        public void run() {
                            try {
                                String message = new String(data, "UTF-8");
                                publisherSubscriberHandlerListeners.receivedMessage(message,topicName);
                            } catch (UnsupportedEncodingException e) {
                                Log.e("PublisherSubscriberHandler", e.getMessage());
                            }
                        }
                    });
                }
            });
        } catch (Exception e) {
            Log.e("PublisherSubscriberHandler ", e.getMessage());
        }
    }

    @SuppressLint("LongLogTag")
    void unSubscribeCurrentTopicAndSubscribeNewTopic(AWSIotMqttManager mqttManager, String topicName, String newTopicName) {
        try {
            mqttManager.unsubscribeTopic(topicName);
            subscribeTopic(mqttManager, newTopicName);
        } catch (Exception e) {
            Log.e("PublisherSubscriberHandler ", e.getMessage());
        }
    }

    @SuppressLint("LongLogTag")
    void publishMessageToTopic(AWSIotMqttManager mqttManager, String publishMessage, String publishTopicName) {
        try {
            mqttManager.publishString(publishMessage, publishTopicName, AWSIotMqttQos.QOS0);
        } catch (Exception e) {
            Log.e("PublisherSubscriberHandler", e.getMessage());
        }
    }
}
